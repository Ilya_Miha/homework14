﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Security;
using System.Data.SqlClient;

namespace HW14
{
    class SqlConnector
    {
        private readonly SqlCredential _credential;
        public string _connectionString;

        public SqlConnector()
        {

        }

        public SqlConnector(string login, string password)
        {
            // convert password
            var credential = new SecureString();
            for (var i = 0; i < password.Length; i++)
                credential.InsertAt(i, password[i]);

            credential.MakeReadOnly();

            //save your Credential for requests
            _credential = new SqlCredential(login, credential);
        }
        public void ConnectToCatalog(string catalogName)
        {
            _connectionString = "Data Source = DESKTOP-3T1T60K; " + $"Initial Catalog = {catalogName}; ";
        }

        public DataTable Execute(string sqlRequest)
        {
            //request result
            var dataSet = new DataSet();

            using (SqlConnection sqlConnection = new SqlConnection(_connectionString, _credential))
            {
                sqlConnection.Open();
                //create sql request
                using (SqlCommand sqlCommand = new SqlCommand(sqlRequest, sqlConnection))
                {
                    sqlCommand.CommandText = sqlRequest;
                    //execute sql request
                    var adapter = new SqlDataAdapter(sqlCommand);
                    //Save result
                    adapter.Fill(dataSet);
                }
            }

            if (sqlRequest.StartsWith("SELECT") && dataSet.Tables[0].Rows[0] != null)
            {
                return dataSet.Tables[0];
            }
            else
            {
                throw new System.IndexOutOfRangeException("There is no row at position 0.");
            }
        }
    }
}
