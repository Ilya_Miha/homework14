﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace HW14
{
    [TestFixture]
    public class TestMain
    {

        SqlConnector sqlConnector = new SqlConnector("mihailya", "test123");

        [Test]
        public void CheckNameOfColumsInPersonsTable()
        {
            sqlConnector.ConnectToCatalog("TestDB");
            string resultID = sqlConnector.Execute("SELECT * FROM Persons").Columns[0].ColumnName.ToString();
            Assert.AreEqual("ID", resultID);
            string resultFirstName = sqlConnector.Execute("SELECT * FROM Persons").Columns[1].ColumnName.ToString();
            Assert.AreEqual("FirstName", resultFirstName);
            string resultLastName = sqlConnector.Execute("SELECT * FROM Persons").Columns[2].ColumnName.ToString();
            Assert.AreEqual("LastName", resultLastName);
            string resultAge = sqlConnector.Execute("SELECT * FROM Persons").Columns[3].ColumnName.ToString();
            Assert.AreEqual("Age", resultAge);
            string resultCity = sqlConnector.Execute("SELECT * FROM Persons").Columns[4].ColumnName.ToString();
            Assert.AreEqual("City", resultCity);
        }

        [Test]
        public void CheckNameOfColumsInOrdersTable()
        {
            sqlConnector.ConnectToCatalog("TestDB");
            string resultID = sqlConnector.Execute("SELECT * FROM Orders").Columns[0].ColumnName.ToString();
            Assert.AreEqual("ID", resultID);
            string resultFirstName = sqlConnector.Execute("SELECT * FROM Orders").Columns[1].ColumnName.ToString();
            Assert.AreEqual("FirstName", resultFirstName);
            string resultLastName = sqlConnector.Execute("SELECT * FROM Orders").Columns[2].ColumnName.ToString();
            Assert.AreEqual("LastName", resultLastName);
            string resultPrice = sqlConnector.Execute("SELECT * FROM Orders").Columns[3].ColumnName.ToString();
            Assert.AreEqual("Price", resultPrice);
        }

        [Test]
        public void CheckAvarageSumOfOrderPrice()
        {
            sqlConnector.ConnectToCatalog("TestDB");
            object[] resultAVG = sqlConnector.Execute("SELECT AVG(Orders.Price) AS AVG_orderPrice FROM Orders").Rows[0].ItemArray;
            string result = resultAVG[0].ToString();
            Assert.AreEqual("2166,9000", result);
        }

        [Test]
        public void CheckCountInPersonsTable()
        {
            sqlConnector.ConnectToCatalog("TestDB");
            int result = sqlConnector.Execute($"SELECT * FROM Persons").Rows.Count;
            Assert.AreEqual(20, result);
        }

        [Test]
        public void CheckCountInOrdersTable()
        {
            sqlConnector.ConnectToCatalog("TestDB");
            int result = sqlConnector.Execute($"SELECT * FROM Orders").Rows.Count;
            Assert.AreEqual(10, result);
        }

        [Test]
        public void CheckMaxAgeInPersonsTable()
        {
            sqlConnector.ConnectToCatalog("TestDB");
            object[] resultMax = sqlConnector.Execute("SELECT MAX(AGE) AS maxAge FROM Persons").Rows[0].ItemArray;
            string result = resultMax[0].ToString();
            Assert.AreEqual("54", result);
        }

        [Test]
        public void CheckMinAgeInPersonsTable()
        {
            sqlConnector.ConnectToCatalog("TestDB");
            object[] resultMax = sqlConnector.Execute("SELECT MIN(AGE) AS minAge FROM Persons").Rows[0].ItemArray;
            string result = resultMax[0].ToString();
            Assert.AreEqual("18", result);
        }

        [Test]
        public void CheckAgeMore18InPersonsTable()
        {
            bool flag = false;
            sqlConnector.ConnectToCatalog("TestDB");
            DataTable result = sqlConnector.Execute("SELECT*FROM Persons");
            for (int i = 0; i < result.Rows.Count; i++)
            {
                if (int.Parse(result.Rows[i].ItemArray[3].ToString()) < 18)
                {
                    flag = true;
                    break;
                }
                else
                {
                    flag = false;
                }
            }
            Assert.AreEqual(false, flag);
        }

        [Test]
        public void CheckPersonNameInOrdersTable()
        {
            sqlConnector.ConnectToCatalog("TestDB");
            object[] resultName = sqlConnector.Execute("SELECT * FROM Orders WHERE FirstName LIKE 'Ilya'").Rows[1].ItemArray;
            string result = resultName[1].ToString();
            Assert.AreEqual("Ilya", result);
        }

        [Test]
        public void CheckSumOfOrderPrice()
        {
            sqlConnector.ConnectToCatalog("TestDB");
             object[] resultAVG = sqlConnector.Execute("SELECT SUM(Orders.Price) AS SUM_orderPrice FROM Orders").Rows[0].ItemArray;
            string result = resultAVG[0].ToString();
            Assert.AreEqual("21669,0000", result);
        }
    }
}
